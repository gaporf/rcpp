import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CallChainTest {
    @Test
    void emptyString() {
        assertEquals("filter{(element=element)}%>%map{element}", CallChain.toFilterMapCall(""));
    }

    @Test
    void onlyFilter() {
        assertEquals("filter{(element=5)}%>%map{element}",
                CallChain.toFilterMapCall("filter{(element=5)}"));
    }

    @Test
    void manyArgumentsInFiler() {
        assertEquals("filter{((element=5)|(element=6))}%>%map{element}",
                CallChain.toFilterMapCall("filter{((element=5)|(element=6))}"));
    }

    @Test
    void manyFilters() {
        assertEquals("filter{(((element=5)&(element>0))&(element=3))}%>%map{element}",
                CallChain.toFilterMapCall("filter{(element=5)}%>%filter{(element>0)}%>%filter{(element=3)}"));
    }

    @Test
    void onlyMap() {
        assertEquals("filter{(element=element)}%>%map{(2*element)}", CallChain.toFilterMapCall("map{(2*element)}"));
    }

    @Test
    void manyArgumentsInMap() {
        assertEquals("filter{(element=element)}%>%map{((2+element)*element)}", CallChain.toFilterMapCall("map{((2+element)*element)}"));
    }

    @Test
    void manyMaps() {
        assertEquals("filter{(element=element)}%>%map{(((2*element)+(2*element))*((2*element)+(2*element)))}",
                CallChain.toFilterMapCall("map{(2*element)}%>%map{(element+element)}%>%map{(element*element)}"));
    }

    @Test
    void filterMap() {
        assertEquals("filter{(element>0)}%>%map{(element*-2)}",
                CallChain.toFilterMapCall("filter{(element>0)}%>%map{(element*-2)}"));
    }

    @Test
    void example1() {
        assertEquals("filter{((element>10)&(element<20))}%>%map{element}",
                CallChain.toFilterMapCall("filter{(element>10)}%>%filter{(element<20)}"));
    }

    @Test
    void example2() {
        assertEquals("filter{((element+10)>10)}%>%map{((element+10)*(element+10))}",
                CallChain.toFilterMapCall("map{(element+10)}%>%filter{(element>10)}%>%map{(element*element)}"));
    }

    @Test
    void example4MissedBracket() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("filter{(element>0)}%>%filter{(element<0)}%>%map{(element*element)"));
    }

    @Test
    void example4Right() {
        assertEquals("filter{((element>0)&(element<0))}%>%map{(element*element)}",
                CallChain.toFilterMapCall("filter{(element>0)}%>%filter{(element<0)}%>%map{(element*element)}"));
    }

    @Test
    void errorElement() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("filter{(elem=element)}"));
    }

    @Test
    void errorOperator() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("map{(element^2)}"));
    }

    @Test
    void missedBracket() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("map{(element*2}"));
    }

    @Test
    void incorrectMap() {
        assertThrows(CallChain.TypeError.class,
                () -> CallChain.toFilterMapCall("map{(element>2)}"));
    }

    @Test
    void incorrectFilter1() {
        assertThrows(CallChain.TypeError.class,
                () -> CallChain.toFilterMapCall("filter{(element+2)}"));
    }

    @Test
    void incorrectFilter2() {
        assertThrows(CallChain.TypeError.class,
                () -> CallChain.toFilterMapCall("filter{(element|element)}"));
    }

    @Test
    void missedFigureBracket() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("map{element%>%filter{(element=5)}"));
    }

    @Test
    void missedFigureBracket2() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("filter{(element>0)%>%map{element}"));
    }

    @Test
    void errorMp() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("mp{element}"));
    }

    @Test
    void errorFlter() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("flter{(element=2)}"));
    }

    @Test
    void filterElement() {
        assertThrows(CallChain.TypeError.class,
                () -> CallChain.toFilterMapCall("filter{element}"));
    }

    @Test
    void transform() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("transform{(element*element)}"));
    }

    @Test
    void forgotDelimiter() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("map{element}%>map{(element*2)}"));
    }

    @Test
    void incomplete() {
        assertThrows(CallChain.SyntaxError.class,
                () -> CallChain.toFilterMapCall("map{(element*25"));
    }

    @Test
    void simplifyConst() {
        assertEquals("filter{(element>0)}%>%map{(6+element)}",
                CallChain.toFilterMapCall("filter{(element>0)}%>%map{((2+(2*(3-1)))+element)}"));
    }
}