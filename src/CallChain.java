import java.math.BigInteger;
import java.util.function.BiFunction;

/**
 * @author Nikolai Akimov
 */

public class CallChain {
    public static class ParserException extends RuntimeException {
        public ParserException(String message) {
            super(message);
        }
    }

    public static class SyntaxError extends ParserException {
        public SyntaxError() {
            super("Syntax error");
        }
    }

    public static class TypeError extends ParserException {
        public TypeError() {
            super("Type error");
        }
    }

    private static class State {
        private String string;
        int curIndex;

        public State(String string) {
            this.string = string;
            curIndex = 0;
        }

        boolean hasNextChar() {
            return curIndex < string.length();
        }

        char showChar() {
            if (curIndex >= string.length()) {
                throw new SyntaxError();
            }
            return string.charAt(curIndex);
        }

        char getNextChar() {
            return getNextChars(1).charAt(0);
        }

        String getNextChars(int n) {
            if (curIndex + n > string.length()) {
                throw new SyntaxError();
            }
            String ans = string.substring(curIndex, curIndex + n);
            curIndex += n;
            return ans;
        }
    }

    private interface Expression {
        String getName();
    }

    private static class ConstantExpression implements Expression {
        private BigInteger value;

        public ConstantExpression(BigInteger value) {
            this.value = value;
        }

        public ConstantExpression(String num) {
            value = new BigInteger(num);
        }

        @Override
        public String getName() {
            return value.toString();
        }
    }

    private static ConstantExpression parseConstant(State state) {
        StringBuilder num = new StringBuilder();
        if (state.showChar() == '-') {
            num.append(state.getNextChar());
        }
        while (Character.isDigit(state.showChar())) {
            num.append(state.getNextChar());
        }
        return new ConstantExpression(num.toString());
    }

    private static class InitialElement implements Expression {
        @Override
        public String getName() {
            return "element";
        }
    }

    private static class Element implements Expression {
        private Expression element;

        public Element(Expression element) {
            this.element = element;
        }

        @Override
        public String getName() {
            return element.getName();
        }
    }

    private static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '>' || c == '<' || c == '=' || c == '&' || c == '|';
    }

    private static Element parseElement(State state, Expression expression) {
        String element = state.getNextChars(7);
        if (!element.equals("element")) {
            throw new SyntaxError();
        }
        return new Element(expression);
    }

    private interface Operator {
        char getName();
    }

    private static class MapOperator implements Operator {
        char op;

        public MapOperator(char op) {
            this.op = op;
        }

        @Override
        public char getName() {
            return op;
        }
    }

    private static class CompareOperator implements Operator {
        char op;

        public CompareOperator(char op) {
            this.op = op;
        }

        @Override
        public char getName() {
            return op;
        }
    }

    private static class FilterOperator implements Operator {
        char op;

        public FilterOperator(char op) {
            this.op = op;
        }

        @Override
        public char getName() {
            return op;
        }
    }

    private static Operator getOperator(char op) {
        return (op == '+' || op == '-' || op == '*') ? new MapOperator(op) :
                (op == '<' || op == '>' || op == '=') ? new CompareOperator(op) : new FilterOperator(op);
    }

    private static class BinaryExpression implements Expression {
        private Expression left, right;
        private Operator op;

        public BinaryExpression(Expression left, Operator op, Expression right) {
            this.left = left;
            this.op = op;
            this.right = right;
        }

        public Operator getOperator() {
            return op;
        }

        public Expression getLeft() {
            return left;
        }

        public Expression getRight() {
            return right;
        }

        @Override
        public String getName() {
            return '(' + left.getName() + op.getName() + right.getName() + ')';
        }
    }

    private static Expression parseExpression(State state, Expression expression) {
        char curChar = state.showChar();
        if (curChar == '(') {
            state.getNextChar();
            Expression left = parseExpression(state, expression);
            char op = state.getNextChar();
            if (!isOperator(op)) {
                throw new SyntaxError();
            }
            Expression right = parseExpression(state, expression);
            if (state.getNextChar() != ')') {
                throw new SyntaxError();
            }
            return new BinaryExpression(left, getOperator(op), right);
        } else if (curChar == 'e') {
            return parseElement(state, expression);
        } else {
            return parseConstant(state);
        }
    }

    private static BiFunction<ConstantExpression, ConstantExpression, ConstantExpression> getFunction(char op) {
        if (op == '*') {
            return (a, b) -> new ConstantExpression(a.value.multiply(b.value));
        } else if (op == '+') {
            return (a, b) -> new ConstantExpression(a.value.add(b.value));
        } else {
            return (a, b) -> new ConstantExpression(a.value.subtract(b.value));
        }
    }

    private static Expression simplify(Expression e) {
        if (e instanceof BinaryExpression) {
            BinaryExpression binaryExpression = (BinaryExpression) e;
            Expression left = simplify(binaryExpression.getLeft());
            Expression right = simplify(binaryExpression.getRight());
            Operator operator = binaryExpression.getOperator();
            if (operator instanceof MapOperator) {
                if (left instanceof ConstantExpression && right instanceof ConstantExpression) {
                    return getFunction(operator.getName()).apply((ConstantExpression) left, (ConstantExpression) right);
                }
            }
            return new BinaryExpression(left, operator, right);
        } else {
            return e;
        }
    }

    private interface Call {
        String getName();

        Expression getExpression();
    }

    private static boolean isCorrectMap(Expression expression) {
        if (expression instanceof BinaryExpression) {
            BinaryExpression binaryExpression = (BinaryExpression) expression;
            if (binaryExpression.getOperator() instanceof CompareOperator) {
                return false;
            }
            return isCorrectMap(binaryExpression.getLeft()) && isCorrectMap(binaryExpression.getRight());
        } else {
            return true;
        }
    }

    private static class MapCall implements Call {
        private Expression expression;

        public MapCall(Expression expression) {
            this.expression = expression;
            if (!isCorrectMap(expression)) {
                throw new TypeError();
            }
        }

        @Override
        public String getName() {
            return "map{" + simplify(expression).getName() + '}';
        }

        @Override
        public Expression getExpression() {
            return expression;
        }
    }

    private static boolean isCorrectFilter(Expression expression) {
        if (expression instanceof BinaryExpression) {
            BinaryExpression binaryExpression = (BinaryExpression) expression;
            if (binaryExpression.getOperator() instanceof CompareOperator) {
                return isCorrectMap(binaryExpression.getLeft()) && isCorrectMap(binaryExpression.getRight());
            } else if (binaryExpression.getOperator() instanceof FilterOperator) {
                return isCorrectFilter(binaryExpression.getLeft()) && isCorrectFilter(binaryExpression.getRight());
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static class FilterCall implements Call {
        private Expression expression;

        public FilterCall(Expression expression) {
            this.expression = expression;
            if (!isCorrectFilter(expression)) {
                throw new TypeError();
            }
        }

        @Override
        public String getName() {
            return "filter{" + simplify(expression).getName() + '}';
        }

        @Override
        public Expression getExpression() {
            return expression;
        }
    }

    private static Call parseCall(State state, Expression expression) {
        char curChar = state.showChar();
        if (curChar == 'm') {
            String isMap = state.getNextChars(4);
            if (isMap.equals("map{")) {
                Expression e = parseExpression(state, expression);
                if (state.getNextChar() != '}') {
                    throw new SyntaxError();
                }
                return new MapCall(e);
            } else {
                throw new SyntaxError();
            }
        } else if (curChar == 'f') {
            String isFilter = state.getNextChars(7);
            if (isFilter.equals("filter{")) {
                Expression e = parseExpression(state, expression);
                if (!(e instanceof BinaryExpression)) {
                    throw new TypeError();
                }
                if (state.getNextChar() != '}') {
                    throw new SyntaxError();
                }
                return new FilterCall(e);
            } else {
                throw new SyntaxError();
            }
        } else {
            throw new SyntaxError();
        }
    }

    public static String toFilterMapCall(String callChain) {
        State state = new State(callChain);
        Expression element = new InitialElement();
        Call filterCall = null;
        Call mapCall = new MapCall(new InitialElement());
        while (state.hasNextChar()) {
            Call call = parseCall(state, element);
            if (call instanceof FilterCall) {
                filterCall = (filterCall == null ? call :
                        new FilterCall(new BinaryExpression(filterCall.getExpression(), new FilterOperator('&'), call.getExpression())));
            } else {
                mapCall = call;
                element = mapCall.getExpression();
            }
            if (state.hasNextChar()) {
                String delimiter = state.getNextChars(3);
                if (!delimiter.equals("%>%")) {
                    throw new SyntaxError();
                }
            }
        }
        if (filterCall == null) {
            filterCall = new FilterCall(new BinaryExpression(new InitialElement(), new CompareOperator('='), new InitialElement()));
        }
        return filterCall.getName() + "%>%" + mapCall.getName();
    }
}